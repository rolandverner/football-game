# Steps to run
1) composer install
2) cp .env.example .env
3) ./vendor/bin/sail up -d
4) ./vendor/bin/sail artisan key:generate
5) ./vendor/bin/sail artisan config:cache
6) ./vendor/bin/sail artisan migrate --seed
7) http://localhost/



# remove volumes
./vendor/bin/sail down -v
