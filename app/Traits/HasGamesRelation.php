<?php

namespace App\Traits;

use App\Models\Game;
use Illuminate\Database\Eloquent\Relations\HasMany;


trait HasGamesRelation
{
    /**
     * @return HasMany
     */
    public function games(): HasMany
    {
        return $this->hasMany(Game::class);
    }

}
