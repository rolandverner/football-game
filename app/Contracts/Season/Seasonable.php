<?php

namespace App\Contracts\Season;

use App\Models\Season;
use Illuminate\Database\Eloquent\Model;

interface Seasonable
{
    /**
     * Season entity.
     *
     * @return Season|Model
     */
    public function generateSeason(): Season|Model;

}
