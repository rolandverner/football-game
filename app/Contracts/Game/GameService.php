<?php

namespace App\Contracts\Game;

use App\Models\Season;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface GameService
{
    /**
     * Club entity.
     *
     * @param Season $season
     * @param int $weak
     * @return void
     */
    public function play(Season $season, int $weak);


    /**
     * @return void
     */
    public function genGame(): void;

    /**
     * @param Season $season
     * @param int $week
     * @param array $clubs
     */
    public function playWithEachOther(Season $season, int $week, array $clubs);
}
