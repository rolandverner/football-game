<?php

namespace App\Providers;

use App\Contracts\Game\GameContract;
use App\Contracts\Game\GameService as GameServiceContract;
use App\Models\Game;
use App\Services\GameService;
use Illuminate\Support\ServiceProvider;

class GameServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerContracts();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register Seasons classes in the container.
     *
     * @return void
     */
    protected function registerContracts(): void
    {
        $this->app->bind(GameContract::class, Game::class);
        $this->app->singleton(GameServiceContract::class, GameService::class);
    }
}
