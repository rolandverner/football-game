<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('season_id')->nullable();
            $table->tinyInteger('week')->default(1);
            $table->unsignedBigInteger('club_first_id')->nullable();
            $table->unsignedBigInteger('club_second_id')->nullable();
            $table->tinyInteger('club_first_goals')->default(0);
            $table->tinyInteger('club_second_goals')->default(0);
            $table->boolean('played')->default(0);
            $table->timestamps();

            $table->foreign('season_id')
                ->references('id')
                ->on('seasons')
                ->onDelete('cascade');

            $table->foreign('club_first_id')
                ->references('id')
                ->on('clubs')
                ->onDelete('cascade');

            $table->foreign('club_second_id')
                ->references('id')
                ->on('clubs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
