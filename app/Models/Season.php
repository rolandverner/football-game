<?php

namespace App\Models;

use App\Contracts\Season\SeasonContract;
use App\Traits\Gameable;
use Database\Factories\SeasonFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Season
 *
 * @property-read Collection|Game[] $games
 * @property-read int|null $games_count
 * @method static SeasonFactory factory(...$parameters)
 * @method static Builder|Season newModelQuery()
 * @method static Builder|Season newQuery()
 * @method static Builder|Season query()
 * @mixin Eloquent
 */
class Season extends Model implements SeasonContract
{
    use HasFactory;
    use Gameable;

    protected $fillable = ['name', 'started_at', 'ended_at'];
}
