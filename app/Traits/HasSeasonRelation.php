<?php

namespace App\Traits;

use App\Contracts\Season\SeasonContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


trait HasSeasonRelation
{
    /**
     * Entity Seasons.
     *
     * @return BelongsTo
     */
    public function season(): BelongsTo
    {
        return $this->belongsTo(app(SeasonContract::class));
    }

}
