<?php

namespace App\Http\Livewire;

use App\Contracts\Club\ClubRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Games extends Component
{

    private ClubRepositoryInterface $clubRepository;

    /**
     * @param ClubRepositoryInterface $clubRepository
     * @return void
     */
    public function boot(ClubRepositoryInterface $clubRepository)
    {
        $this->clubRepository = $clubRepository;
    }

    /**
     * @return Factory|View|Application
     */
    public function render(): Factory|View|Application
    {
        $clubs = $this->clubRepository->getClubs();
        return view('livewire.games', [
            'clubs' => $clubs,
        ]);
    }
}
