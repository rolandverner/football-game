<?php

namespace App\Contracts\Club;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface Clubbable
{
    /**
     * Club entity.
     *
     * @param bool $paginate
     * @return Collection|LengthAwarePaginator|array
     */
    public function getClubs(bool $paginate): Collection|LengthAwarePaginator|array;

    /**
     * @return array
     */
    public function getClubsIds(): array;

    /**
     * @return int
     */
    public function getClubsCount(): int;

}
