<?php

namespace App\Traits;

use App\Contracts\Season\SeasonService as SeasonServiceContract;
use App\Models\Season;
use Illuminate\Database\Eloquent\Model;


trait HasSeasonHelper
{
    /**
     * Season model.
     *
     * @return Season|Model
     */
    public function generateSeason(): Season|Model
    {
        return app(SeasonServiceContract::class)->generateSeason();
    }
}
