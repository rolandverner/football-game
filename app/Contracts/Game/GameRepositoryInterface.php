<?php

namespace App\Contracts\Game;

use App\Models\Club;
use App\Models\Game;
use App\Models\Season;
use Illuminate\Database\Eloquent\Collection as CollectionAlias;
use Illuminate\Database\Eloquent\Model;

interface GameRepositoryInterface
{
    /**
     * @param Season $season
     * @param bool $weak
     * @return void
     */
    public function setGoal(Season $season, bool $weak): void;

    /**
     * @param Season $season
     * @param array $gameParameters
     * @return Model
     */
    public function addGames(Season $season, array $gameParameters): Model;


    /**
     * @return int
     */
    public function genGoal(): int;

    /**
     * @param Game $game
     * @return Club|null
     */
    public function getWinner(Game $game): ?Club;

    /**
     * @param Season $season
     * @param int $week
     * @return CollectionAlias
     */
    public function getAllPlayedWeeks(Season $season, int $week): CollectionAlias;

}
