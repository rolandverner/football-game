<?php

namespace App\Services;

use App\Contracts\Season\SeasonRepositoryInterface;
use App\Contracts\Season\SeasonService as SeasonServiceContract;
use App\Models\Season;
use Illuminate\Database\Eloquent\Model;

class SeasonService implements SeasonServiceContract
{

    private SeasonRepositoryInterface $seasonRepository;

    /**
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }


    /**
     * @return Season|Model
     */
    public function generateSeason(): Season|Model
    {
        $last = $this->seasonRepository->getLatestSeason();
        $season = $this->seasonRepository->createSeason(['name' => 1]);
        if ($last) {
            $this->seasonRepository->incrementSeasonName($season, $last);
        }
        return $season;
    }
}
