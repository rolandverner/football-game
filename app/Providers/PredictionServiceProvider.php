<?php

namespace App\Providers;

use App\Contracts\Prediction\PredictionService as PredictionServiceContract;
use App\Services\PredictionService;
use Illuminate\Support\ServiceProvider;

class PredictionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerContracts();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    protected function registerContracts(): void
    {
        $this->app->singleton(PredictionServiceContract::class, PredictionService::class);
    }
}
