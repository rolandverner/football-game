<?php

namespace App\Models;

use App\Contracts\Club\ClubContract;
use Database\Factories\ClubFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Club
 *
 * @property-read Collection|Game[] $firstGame
 * @property-read int|null $first_game_count
 * @property-read Collection|Game[] $secondGame
 * @property-read int|null $second_game_count
 * @method static ClubFactory factory(...$parameters)
 * @method static Builder|Club newModelQuery()
 * @method static Builder|Club newQuery()
 * @method static Builder|Club query()
 * @mixin Eloquent
 */
class Club extends Model implements ClubContract
{
    use HasFactory;

    public const WIN_POINT = 3;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'logo'];

    /**
     * @return HasMany
     */
    public function firstGame(): HasMany
    {
        return $this->hasMany(Game::class, 'club_first_id');
    }

    /**
     * @return HasMany
     */
    public function secondGame(): HasMany
    {
        return $this->hasMany(Game::class, 'club_second_id');
    }

}
