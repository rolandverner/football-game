<?php

namespace App\Contracts\Season;

use App\Models\Season;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorAlias;
use Illuminate\Database\Eloquent\Collection as CollectionAlias;
use Illuminate\Database\Eloquent\Model;

interface SeasonRepositoryInterface
{
    /**
     * Determine if Season is finished.
     *
     * @return LengthAwarePaginatorAlias
     */
    public function getAllSeasons(): LengthAwarePaginatorAlias;

    /**
     * @return LengthAwarePaginatorAlias
     */
    public function getAllSeasonsSorted(): LengthAwarePaginatorAlias;

    /**
     * @return Season|Model|null
     */
    public function getLatestSeason(): Season|Model|null;

    /**
     * @param array $seasonParams
     * @return Season|Model
     */
    public function createSeason(array $seasonParams): Season|Model;

    /**
     * @param Season $season
     * @param Season $previousSeason
     * @return int|false
     */
    public function incrementSeasonName(Season $season, Season $previousSeason): bool|int;

    /**
     * @param Season $season
     * @return array
     */
    public function getWeeks(Season $season): array;

    /**
     * @param Season $season
     * @return mixed
     */
    public function getPlayedWeeks(Season $season): mixed;

    /**
     * @param Season $season
     * @return array
     */
    public function getLeftWeeks(Season $season): array;

    /**
     * @param Season $season
     * @return false|mixed
     */
    public function getNextWeek(Season $season): mixed;

    /**
     * @param Season $season
     * @return bool
     */
    public function isFinished(Season $season): bool;

}
