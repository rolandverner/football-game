<?php

namespace App\Services;

use App\Contracts\Club\ClubRepositoryInterface;
use App\Contracts\Prediction\PredictionService as PredictionServiceContract;
use App\Models\Club;
use App\Models\Season;

class PredictionService implements PredictionServiceContract
{
    private ClubRepositoryInterface $clubRepository;

    /**
     * @param ClubRepositoryInterface $clubRepository
     */
    public function __construct(
        ClubRepositoryInterface $clubRepository,
    ) {
        $this->clubRepository = $clubRepository;
    }

    /**
     * @param Season $season
     * @param int $week
     * @return array
     */
    public function getPrediction(Season $season, int $week): array
    {
        $clubs = $this->clubRepository->getClubs()->map(fn(Club $club) => [
            'id' => $club->id,
            'name' => $club->name,
            'games' => $this->clubRepository->getGames($club, $season, $week)->count(),
            'points' => $this->clubRepository->getPoints($club, $season, $week),
            'draws' => $this->clubRepository->getDraws($club, $season, $week)->count(),
            'wins' => $this->clubRepository->getWins($club, $season, $week)->count(),
            'lost' => $this->clubRepository->getLost($club, $season, $week)->count(),
            'goals' => [
                'scored' => $this->clubRepository->goalsScored($club, $season, $week),
                'received' => $this->clubRepository->goalsReceived($club, $season, $week),
                'difference' => $this->clubRepository->getGoalDiff($club, $season, $week)
            ]
        ])->toArray();

        //В среднем сколько клуб имеет очков
        $average_points = [];
        foreach ($clubs as $club) {
            $average_points[$club['id']] = $club['games'] ? $club['points'] : 0;
        }

        //В среднем сколько матчей выигрывали клубы
        $average_wins = [];
        foreach ($clubs as $club) {
            $average_wins[$club['id']] = $club['games'] ? $club['wins'] / $club['games'] : 0;
        }

        //В среднем сколько матчей проигрывали
        $average_defeats = [];
        foreach ($clubs as $club) {
            $average_defeats[$club['id']] = $club['games'] ? $club['lost'] / $club['games'] : 0;
        }

        //В среднем сколько матчей играли в ничью
        $average_draws = [];
        foreach ($clubs as $club) {
            $average_draws[$club['id']] = $club['games'] ? $club['draws'] / $club['games'] : 0;
        }

        //Получаем рейтинг нападения. Получить средний показатель забитых голов
        $average_scores = [];
        foreach ($clubs as $club) {
            $average_scores[$club['id']] = $club['games'] ? $club['goals']['scored'] / $club['games'] : 0;
        }

        //Получаем рейтинг защиты. Получить средний показатель пропущенных голов
        $average_received = [];
        foreach ($clubs as $club) {
            $average_received[$club['id']] = $club['games'] ? $club['goals']['received'] / $club['games'] : 0;
        }

        //Получаем рейтинг защиты. Получить средний показатель пропущенных голов
        $average_difference = [];
        foreach ($clubs as $club) {
            $average_difference[$club['id']] = $club['games'] ? $club['goals']['difference'] / $club['games'] : 0;
        }

        //Вероятность что будут забыты
        $scores = [];
        foreach ($clubs as $club) {
            foreach ($average_scores as $sk => $scored) {
                foreach ($average_received as $rk => $received) {
                    if ($club['id'] != $sk && $club['id'] != $rk && $sk != $rk) {
                        $scores[$club['id']][$rk] = ($scored + $received) / 2;
                    }
                }
            }
        }

        /**
         * Теперь можно округлять учитывая коэффициенты выигрышей,
         * проигрышей и ничейных результатов.
         */

        $scores_win = [];

        foreach ($clubs as $club) {
            $filtered = $average_defeats;
            unset($filtered[$club['id']]);
            $scores_win[$club['id']] = ($average_points[$club['id']] + $average_wins[$club['id']] + $average_draws[$club['id']] + $average_difference[$club['id']]) + array_sum(
                    $filtered
                ) / 4;
        }


        $predictions = [];
        foreach ($clubs as $club) {
            foreach ($scores as $sk => $score) {
                foreach ($scores_win as $skw => $score_win) {
                    if ($sk == $skw && $skw == $club['id'] && $sk == $club['id']) {
                        $predictions[$club['name']] = round(array_sum($score) + ($score_win * 4));
                    }
                }
            }
        }
        arsort($predictions);

        $total = array_sum($predictions);
        return array_map(fn(string $value) => number_format(($value / $total) * 100, 2), $predictions);
    }
}
