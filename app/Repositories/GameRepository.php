<?php

namespace App\Repositories;

use App\Contracts\Game\GameRepositoryInterface;
use App\Contracts\Season\SeasonRepositoryInterface;
use App\Models\Club;
use App\Models\Game;
use App\Models\Season;
use Exception;
use Illuminate\Database\Eloquent\Collection as CollectionAlias;
use Illuminate\Database\Eloquent\Model;

class GameRepository implements GameRepositoryInterface
{
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(
        SeasonRepositoryInterface $seasonRepository,
    ) {
        $this->seasonRepository = $seasonRepository;
    }

    public const GOALS_LIMIT_FROM = 0;
    public const GOALS_LIMIT_TO = 5;

    /**
     * @param $season
     * @param bool $weak
     * @return void
     */
    public function setGoal($season, bool $weak = false): void
    {
        try {
            $games = Game::query();
            $games->where('season_id', $season->id);
            $games->where('played', false);
            if ($weak) {
                if ($this->seasonRepository->getNextWeek($season)) {
                    $games->where('week', '=', $this->seasonRepository->getNextWeek($season));
                }
            }
            $games = $games->get();
            foreach ($games as $game) {
                $game->played = true;
                $game->club_first_goals = self::genGoal();
                $game->club_second_goals = self::genGoal();
                $game->save();
            }
        } catch (Exception $exception) {
            \Log::error("Can't add goals to game");
        }
    }

    /**
     * @return int
     * @throws Exception
     */
    public function genGoal(): int
    {
        return random_int(self::GOALS_LIMIT_FROM, self::GOALS_LIMIT_TO);
    }

    /**
     * @param Season $season
     * @param array $gameParameters
     * @return Model
     */
    public function addGames(Season $season, array $gameParameters): Model
    {
        return $season->games()->create($gameParameters);
    }

    /**
     * @param Game $game
     * @return Club|null
     */
    public function getWinner(Game $game): ?Club
    {
        return match (true) {
            $game->club_first_goals > $game->club_second_goals => $game->club_first,
            $game->club_first_goals < $game->club_second_goals => $game->club_second,
            default => null,
        };
    }

    /**
     * @param Season $season
     * @param int $week
     * @return CollectionAlias
     */
    public function getAllPlayedWeeks(Season $season, int $week): CollectionAlias
    {
        return Game::where('season_id', $season->id)
            ->where('played', true)
            ->where('week', $week)
            ->get();
    }
}
