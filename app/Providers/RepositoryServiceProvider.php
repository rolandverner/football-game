<?php

namespace App\Providers;

use App\Contracts\Club\ClubRepositoryInterface;
use App\Contracts\Game\GameRepositoryInterface;
use App\Contracts\Season\SeasonRepositoryInterface;
use App\Repositories\ClubRepository;
use App\Repositories\GameRepository;
use App\Repositories\SeasonRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SeasonRepositoryInterface::class, SeasonRepository::class);
        $this->app->bind(ClubRepositoryInterface::class, ClubRepository::class);
        $this->app->bind(GameRepositoryInterface::class, GameRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
