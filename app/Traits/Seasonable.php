<?php

namespace App\Traits;

trait Seasonable
{
    use HasSeasonHelper;
    use HasSeasonRelation;

}
