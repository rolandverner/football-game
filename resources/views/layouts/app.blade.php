<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        @livewireStyles

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />

        @stack('style')
    </head>
    <body class="antialiased">
        <main>
            <div class="items-top min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0 sm:pb-12">
                {{ $slot }}
            </div>

        </main>

        <!-- BEGIN: JS Assets-->
        @livewireScripts

        <script src="{{ mix('js/app.js') }}"></script>
        @stack('script')
        <!-- END: JS Assets-->
    </body>
</html>
