<?php

namespace App\Services;


use App\Contracts\Club\ClubRepositoryInterface;
use App\Contracts\Game\GameRepositoryInterface;
use App\Contracts\Game\GameService as GameServiceContract;
use App\Contracts\Season\SeasonService as SeasonServiceContract;
use App\Models\Game;
use App\Models\Season;
use Illuminate\Support\Facades\Artisan;


class GameService implements GameServiceContract
{
    private GameRepositoryInterface $gameRepository;
    private SeasonServiceContract $seasonService;
    private ClubRepositoryInterface $clubRepository;

    /**
     * @param GameRepositoryInterface $gameRepository
     * @param SeasonServiceContract $seasonService
     * @param ClubRepositoryInterface $clubRepository
     */
    public function __construct(
        GameRepositoryInterface $gameRepository,
        SeasonServiceContract $seasonService,
        ClubRepositoryInterface $clubRepository
    ) {
        $this->gameRepository = $gameRepository;
        $this->seasonService = $seasonService;
        $this->clubRepository = $clubRepository;
    }

    /**
     * @param $season
     * @param $weak
     * @return void
     */
    public function play($season, $weak)
    {
        $this->gameRepository->setGoal($season, $weak);
    }

    /**
     * @return void
     */
    public function genGame(): void
    {
        $season = $this->seasonService->generateSeason();
        $clubs = $this->clubRepository->getClubsCount();
        if ($clubs == 0) {
            Artisan::call('db:seed');
        }
        foreach (Game::WEEK as $week => $club) {
            foreach ($club as $set) {
                $this->playWithEachOther($season, $week, $set);
            }
        }

        $this->gameRepository->setGoal($season, 1);
    }


    /**
     * @param Season $season
     * @param $week
     * @param $clubs
     * @return void
     */
    public function playWithEachOther(Season $season, $week, $clubs)
    {
        $this->gameRepository->addGames($season, [
            'club_first_id' => $clubs[0],
            'club_second_id' => $clubs[1],
            'week' => $week
        ]);
    }

}
