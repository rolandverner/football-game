<div>

    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800">
        <thead class="bg-gray-50 dark:bg-gray-900">
        <tr>
            <th scope="col" colspan="2" class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                {{ $week }}th Week of predictions of championship
            </th>
        </tr>
        </thead>
        <tbody class="divide-y divide-gray-200 dark:divide-gray-800">
        @foreach($predictions as $key => $value)
            <tr>
                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                    {{ $key }}
                </td>
                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                    {{ $value }} %
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
