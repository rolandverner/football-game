<?php

namespace App\Contracts\Prediction;

use App\Models\Season;

interface PredictionService
{
    /**
     * @param Season $season
     * @param int $week
     * @return array
     */
    public function getPrediction(Season $season, int $week): array;
}
