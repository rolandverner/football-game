<?php

namespace App\Contracts\Season;

use App\Models\Season;
use Illuminate\Database\Eloquent\Model;

interface SeasonService
{
    /**
     * @return Season|Model
     */
    public function generateSeason(): Model|Season;

}
