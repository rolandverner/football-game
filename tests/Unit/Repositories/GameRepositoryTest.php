<?php

namespace Tests\Unit\Repositories;

use App\Models\Season;
use App\Repositories\GameRepository;
use App\Repositories\SeasonRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class GameRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
        $this->seasons = Season::factory()->create();
        $this->seasonRepository = new SeasonRepository();
        $this->gameRepository = new GameRepository($this->seasonRepository);
    }

    /**
     * @return void
     */
    public function test_get_all_played_weeks()
    {
        $latestSeason = $this->seasonRepository->getLatestSeason();
        $game = $latestSeason->games()->create([
            'week' => 1,
            'club_first_id' => 1,
            'club_second_id' => 2,
            'club_first_goals' => 2,
            'club_second_goals' => 1,
            'played' => true
        ]);
        $games = $this->gameRepository->getAllPlayedWeeks($latestSeason, 1);
        $this->assertCount(1, $games);
    }


    /**
     * @return void
     */
    public function test_get_winner()
    {
        $latestSeason = $this->seasonRepository->getLatestSeason();
        $game = $latestSeason->games()->create([
            'week' => 1,
            'club_first_id' => 1,
            'club_second_id' => 2,
            'club_first_goals' => 2,
            'club_second_goals' => 1,
            'played' => true
        ]);

        $winner = $this->gameRepository->getWinner($game);
        $this->assertTrue($winner->id === 1);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws \Exception
     */
    public function test_gen_goal()
    {
        $goal = $this->gameRepository->genGoal();
        $this->assertIsInt($goal);
    }


}
