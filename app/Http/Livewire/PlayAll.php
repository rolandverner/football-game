<?php

namespace App\Http\Livewire;

use App\Contracts\Game\GameService as GameServiceContract;
use App\Models\Season;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PlayAll extends Component
{
    private GameServiceContract $gameService;
    public Season $season;

    /**
     * @param GameServiceContract $gameService
     * @return void
     */
    public function boot(GameServiceContract $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @param Season $season
     * @return void
     */
    public function mount(Season $season)
    {
        $this->season = $season;
    }

    /**
     * @return void
     */
    public function playAllGames()
    {
        $this->gameService->play($this->season, false);
    }


    /**
     * @return Factory|View|Application
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.play-all');
    }
}
